Minesweeper implementation written in Haskell using Threepenny GUI and Functional Reactive Programming.  

Includes an autoplayer to suggest a move to the player, which applies basic logic rules when possible. It will first try to place a flag down on a known bomb location, then clear squares that we know can't contain a bomb. Finally, if we aren't sure it will calculate the probability of each cell having a bomb and pick the least likely.

![Playing](images/playing.png)
![Won](images/won.png)
![Lost](images/lost.png)
