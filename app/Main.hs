module Main where

import qualified Sweeper
import qualified Gui

import Graphics.UI.Threepenny.Core
import GHC.Base (returnIO)
import Control.Exception

main :: IO ()
main = do 
          startGUI defaultConfig Gui.setup

