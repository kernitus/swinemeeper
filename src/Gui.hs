module Gui where

import Sweeper
import Helper
import Player

import Control.Lens ((^.), (.~), (&))
import Control.Monad (void)
import qualified Graphics.UI.Threepenny as UI
import Graphics.UI.Threepenny (set, attr, title, text, value,
                              (#), (#+), (<@), Event, Element, UI, Window, sink, accumB)
import Graphics.UI.Threepenny.Core
import Reactive.Threepenny
import System.Random (getStdGen)
import Data.IORef
import Data.Matrix

setup :: Window -> UI ()
setup win = void $ do
  void $ pure win
    # set title "SwineMeeper"

  gen <- getStdGen
  let state =  Sweeper.initialise 8 gen

  let body = UI.getBody win
  -- Prevent right click from showing context menu
  bodo <- getBody win
  preventDefaultContextMenu bodo

  drawBoard state body

  where
      makeButton :: Minesweeper -> UI Element -> Cell -> UI Element
      makeButton gameState body cell = do
          button <- UI.button #. "button" #+ [string $ show cell] # set (attr "style") "min-width: 30px; min-height: 30px"

          -- Left click on cell
          let leftClick = UI.click button
          let event = showCell <$ leftClick
          cellClick <- accumE cell event

          onEvent cellClick $ \newCell -> do
              drawBoard (updateCellState newCell gameState) body 

          -- Right click on cell
          let rightClick = UI.contextmenu button
          let rightClickEvent = toggleFlag <$ rightClick
          cellRightClick <- accumE cell rightClickEvent

          onEvent cellRightClick $ \newCell -> do
              drawBoard (updateCellState newCell gameState) body

          return button

      showCell :: Cell -> Cell
      showCell initial = if initial^.flagged then initial else initial&shown .~ True

      toggleFlag :: Cell -> Cell
      toggleFlag initial = initial&flagged .~ not (initial^.flagged)
          
      -- Draw the whole interface to screen
      drawBoard :: Minesweeper -> UI Element -> UI ()
      drawBoard state el = do
          buttons <- mapM (mapM (makeButton state el)) (toLists (state^.board))
          boardEl <- UI.span #+ map UI.row (map (map return) buttons)
          bar <- statusBar state
          help <- helpButton state el
          el # set children [help, bar, boardEl]
          return ()

      statusBar :: Minesweeper -> UI Element
      statusBar gameState = UI.input
              # set (attr "readonly") "true"
              # set (attr "style") "text-align: center; min-width: 195px"
              # set value (show (gameState^.status) )

      helpButton :: Minesweeper -> UI Element -> UI Element
      helpButton gameState el = do
          button <- UI.button #. "button" #+ [string "Help"]

          -- Help button left click
          let leftClick = UI.click button
          let event = autoPlay <$ leftClick
          cellClick <- accumE gameState event

          onEvent cellClick $ \newGameState -> do
              drawBoard (updateState newGameState) el

          return button

      -- Prevent right click from showing the context menu
      preventDefaultClass = "__prevent-default-context-menu"
      preventDefaultContextMenu :: Element -> UI ()
      preventDefaultContextMenu el = do
          element el # set UI.class_ preventDefaultClass
          runFunction $ ffi "$(%1).bind('contextmenu', e => e.preventDefault())"
                ("." ++ preventDefaultClass)
