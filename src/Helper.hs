module Helper where

import Sweeper

import Control.Lens
import Data.Maybe
import Data.Matrix
import Data.List
import GHC.Float

autoPlay :: Minesweeper -> Minesweeper
autoPlay initial = if isNothing suggestedMove then initial else (setCell initial (fromJust suggestedMove))
    where
        suggestedMove = suggestMove (initial^.board) 

suggestMove :: Board -> Maybe Cell
suggestMove board = listToMaybe (showableCells ++ flaggableCells ++ probabilityCells)
    where
        flaggableCells = flagRule board
        showableCells = unshownFlaggedRule board
        probabilityCells = probabilityRule board

-- Find cells that certainly contain bombs and flag them
flagRule :: Board -> [Cell]
flagRule board = fmap (&flagged .~ True) flaggable
    where
        flaggable = concat $ fmap (flaggableNeighbours board) shownList
        shownList = filter (^.shown) (toList board)

flaggableNeighbours :: Board -> Cell -> [Cell]
flaggableNeighbours board cell = if cell^.neighbouringBombs == length unshownNeighbours then unshownUnflaggedNeighbours else []
    where
        unshownUnflaggedNeighbours = filter (not.(^.flagged)) unshownNeighbours
        unshownNeighbours = filter (not.(^.shown)) (getNeighbours board cell)

-- Find cells that certainly don't have bombs because they've all been flagged
unshownFlaggedRule :: Board -> [Cell]
unshownFlaggedRule board = fmap (&shown.~True) showable
    where
        showable = concat $ fmap (showableNeighbours board) (toList board)

showableNeighbours :: Board -> Cell -> [Cell]
showableNeighbours board cell = if cell ^.shown && cell^.neighbouringBombs == length flaggedNeighbours then unshownUnflaggedNeighbours else []
    where
         flaggedNeighbours = filter (^.flagged) neighbours
         unshownUnflaggedNeighbours = filter (\c -> not (c^.shown) && not (c^.flagged)) neighbours
         neighbours = getNeighbours board cell

probabilityRule :: Board -> [Cell]
probabilityRule board = fmap (&shown.~True) sortedProbabilities
    where
        sortedProbabilities = sort $ filter (not.(^.shown)) (toList $ calculateProbabilities board)

-- Calculate the likelyhood of each unshown cell to have a bomb
calculateProbabilities :: Board -> Board
calculateProbabilities board = foldr addProbability board shownList
    where
        shownList = filter (^.shown) (toList board)

-- Calculate probabilities of neighbours of shown cells and add to sum
addProbability :: Cell -> Board -> Board
addProbability cell board = setCellsInBoard board updatedNeighbours
    where
        updatedNeighbours = fmap (&probability+~addedProbability) unshownNeighbours
        addedProbability = (int2Double (cell^.neighbouringBombs)) / (int2Double (length unshownNeighbours))
        unshownNeighbours = filter (not.(^.shown)) (getNeighbours board cell)
