module Player where

import Sweeper

import Control.Lens
import Control.Monad.State
import System.Random (randomRs, StdGen)
import Util (chunkList, count)
import Data.Maybe
import Data.List
import Data.Matrix

-- Update state when a move is made
-- Show zero flood cells and update status
updateState :: Minesweeper -> Minesweeper
updateState initial = foldr zeroFlood updatedState cells
    where
        cells = toList (updatedState^.board)
        updatedState = updateStatus initial

updateCellState :: Cell -> Minesweeper -> Minesweeper
updateCellState cell initial = updateState (setCell initial cell)

-- Update status and also show all cells if won or lost
updateStatus :: Minesweeper -> Minesweeper
updateStatus initial = if not (wonOrLost initStatus) then
                          (if wonOrLost newStatus then
                              setCells newState (fmap (&shown.~True) (toList $ initial^.board))
                           else newState)
                      else initial
    where
        newState = initial & status .~ newStatus
        newStatus = calculateStatus (initial^.board)
        initStatus = initial^.status
        wonOrLost s = s == Won || s == Lost

-- If a bomb is shown, we lost. If everything is shown but the bombs, we won
-- otherwise, status is total number of bombs - flags placed
calculateStatus :: Board -> GameStatus
calculateStatus board 
    | hasLost board = Lost
    | hasWon board = Won
    | otherwise = Playing $ remainingBombs board

hasLost :: Board -> Bool
hasLost board = any (\c -> c^.shown && c^.bomb) board

hasWon :: Board -> Bool
hasWon board = all (\c -> (not (c^.bomb) && (c^.shown)) || (c^.bomb && not (c^.shown))) board

remainingBombs :: Board -> Int
remainingBombs board = (countBombs list) - (count (^.flagged) list) 
    where list = toList board

-- If we click on a cell with 0 neighbouring mines, expose all neighbours, recursively
zeroFlood :: Cell -> Minesweeper -> Minesweeper
zeroFlood cell initial = if cell ^.shown && not (cell^.flagged) && (cell^.neighbouringBombs <= 0) then showZeroCells initial cell else initial

showZeroCells :: Minesweeper -> Cell -> Minesweeper
showZeroCells initial cell = setCells initial shownZeroCells
    where
        shownZeroCells = fmap (&shown.~True) (neighbours ++ zeroCells)
        neighbours = filter (not.(^.flagged)) (concat $ fmap (getNeighbours b) zeroCells)
        zeroCells = getZeroCells b [cell]
        b = initial^.board

-- Resursively search all non-searched zero-neighbours of given cells until we find no more
getZeroCells :: Board -> [Cell] -> [Cell]
getZeroCells board [] = []
getZeroCells board (c:cs) = zeroNeighbours ++ (getZeroCells markedBoard cs) ++ (getZeroCells markedBoard zeroNeighbours)
    where
        markedBoard = markReachedZeroCells board zeroNeighbours
        zeroNeighbours = filter filterByZero $ getNeighbours board c

-- Mark the reached zero cells so we don't recurse indefinitely
markReachedZeroCells :: Board -> [Cell] -> Board
markReachedZeroCells board cells = setCellsInBoard board markedCells
    where markedCells = fmap (&neighbouringBombs .~ 99) cells

filterByZero :: Cell -> Bool
filterByZero cell = not ((cell^.shown) || (cell^.bomb) || (cell^.flagged)) && (cell^.neighbouringBombs <= 0)



