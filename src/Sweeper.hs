{-# LANGUAGE TemplateHaskell #-}
module Sweeper where

import Control.Lens
import Control.Monad.State
import System.Random (randomRs, StdGen)
import Util (chunkList, count)
import Data.Maybe
import Data.List
import Data.Matrix
import Data.Universe.Helpers ((+*+))

type Position = (Int, Int)

data Cell = Cell
    { _bomb :: Bool
    , _shown :: Bool
    , _neighbouringBombs :: Int
    , _position :: Position
    , _flagged :: Bool
    , _probability :: Double
    }
    deriving Eq

makeLenses ''Cell

instance Show Cell where
    show cell = if (cell^.shown) then (
                    if (cell^.bomb) then "💣" else show (cell^.neighbouringBombs))
                    else (if (cell^.flagged) then "🚩" else ".")

instance Ord Cell where
    compare x y = compare (x^.probability) (y^.probability)

type Board = Matrix Cell
data GameStatus = Won | Lost | Playing Int
    deriving (Show, Eq)

data Minesweeper = Minesweeper
    { _size  :: Int
    , _board :: Board
    , _status :: GameStatus
    }

makeLenses ''Minesweeper

initialise :: Int -> StdGen -> Minesweeper
initialise size stdGen = Minesweeper size board (Playing (countBombs $ toList board))
    where
        board = genBoard size stdGen

genBoard :: Int -> StdGen -> Board
genBoard size stdGen = fromList size size updatedList
    where
        updatedList = fmap (updateNeighbouringBombs board) (toList board)
        board = matrix size size (genCell stdGen size)

genCell :: StdGen -> Int -> (Int,Int) -> Cell
genCell gen size pos = Cell (genRand (randomBools gen size) size pos) False 0 pos False 0

-- Generate larger space so chance of getting a bomb is 18%
randomBools :: StdGen -> Int -> [Bool]
randomBools gen size = fmap (> (8 :: Int)) $ take (size*size) $ randomRs (0,10) gen

-- Get corresponding random number for given cell position
-- Necessary because we are not in IO monad so we can't generate new randomness each time
genRand :: [Bool] -> Int -> (Int,Int) -> Bool
genRand list size pos = list !! (size * (fst pos-1) + (snd pos-1))


-- Update the value of neighbouring bombs for the cell
updateNeighbouringBombs :: Board -> Cell -> Cell
updateNeighbouringBombs board cell = cell & neighbouringBombs .~ nbs
     where nbs = getNeighbouringBombs board cell

getNeighbouringBombs :: Board -> Cell -> Int
getNeighbouringBombs board cell = countBombs (getNeighbours board cell)

-- Count number of bombs in list of cells. Used for getting neighbouring bombs count
countBombs :: [Cell] -> Int
countBombs cells = count (^.bomb) cells

getNeighbours :: Board -> Cell -> [Cell]
getNeighbours board cell = catMaybes $ safeGetCells pos board allOps
    where
        pos = cell^.position
        allOps = take ((length all) - 1) $ all -- we don't need original cell
        all = ops +*+ ops -- Get all combinations of below operators to reach all neighbour's positions
        ops = [(+1),(subtract 1),(+0)]

type IntFunc = (Int -> Int)
type IntFuncPair = (IntFunc,IntFunc)

safeGetCell :: Position -> Board -> IntFuncPair -> Maybe Cell
safeGetCell (row,col) board (rowOp,colOp) = safeGet (rowOp row) (colOp col) board

safeGetCells :: Position -> Board -> [IntFuncPair] -> [Maybe Cell]
safeGetCells pos board ops = fmap (safeGetCell pos board) ops

-- Set cell values
setCell :: Minesweeper -> Cell -> Minesweeper
setCell m cell = m&board .~ setCellInBoard (m^.board) cell

setCells :: Minesweeper -> [Cell] -> Minesweeper
setCells initial [] = initial
setCells initial (c:cs) = setCells (setCell initial c) cs

setCellInBoard :: Board -> Cell -> Board
setCellInBoard b cell = setElem cell (cell^.position) b 

setCellsInBoard :: Board -> [Cell] -> Board 
setCellsInBoard initial [] = initial
setCellsInBoard initial (c:cs) = setCellsInBoard (setCellInBoard initial c) cs
